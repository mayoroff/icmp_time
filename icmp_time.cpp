#include <boost/asio.hpp>
#include <boost/bind.hpp>
#include <istream>
#include <iostream>
#include <ostream>
#include <algorithm>
#include <climits>

#include "boost/date_time/posix_time/posix_time.hpp"
#include "icmp_header.hpp"
#include "ipv4_header.hpp"

using boost::asio::ip::icmp;
using namespace boost::posix_time;

class IcmpTimestamp {
public:
	IcmpTimestamp(): 
	  resolver_(io_service_), 
		  socket_(io_service_, icmp::v4()),
		  sequence_number_(0), 
		  identifier_(::getpid()) {
	  }


	  void server() {
		  server_recv();
		  io_service_.run();
	  }

	  void client(const char * server_addr) {

		  icmp::resolver::query query(icmp::v4(), server_addr, "");
		  icmp::endpoint destination = *resolver_.resolve(query);

		  std::vector<char> time(encodeCurrentTime());

		  std::vector<char> body(12, '\0');
		  std::copy(time.begin(), time.end(), body.begin());
		  send(destination, icmp_header::timestamp_request, 0, body);
		  client_recv();		
		  io_service_.run();
	  }

private:

	void send(boost::asio::ip::icmp::endpoint destination, unsigned char type, unsigned char code, const std::vector<char>& body) {
		icmp_header header;
		header.type(type);
		header.code(code);
		header.identifier(identifier_);
		header.sequence_number(++sequence_number_);
		compute_checksum(header, body.begin(), body.end());

		// Encode the request packet.
		boost::asio::streambuf request_buffer;
		std::ostream os(&request_buffer);

		os << header;
		os.write(body.data(), body.size());

		// Send the request.
		socket_.send_to(request_buffer.data(), destination);
	}

	void client_recv() {
		reply_buffer_.consume(reply_buffer_.size());
		socket_.async_receive(reply_buffer_.prepare(65536),
			boost::bind(&IcmpTimestamp::client_recv_handler, this, _2));
	}

	void client_recv_handler(std::size_t length)
	{
		reply_buffer_.commit(length);

		// Decode the reply packet.
		std::istream is(&reply_buffer_);
		ipv4_header ipv4_hdr;
		icmp_header icmp_hdr;
		is >> ipv4_hdr >> icmp_hdr;

		if (icmp_hdr.type() == icmp_header::timestamp_reply
			&& icmp_hdr.identifier() == identifier_
			&& icmp_hdr.sequence_number() == sequence_number_)
		{
			is.ignore(8);
			unsigned char time[4]; 
			is >> time[3];
			is >> time[2];
			is >> time[1];
			is >> time[0];
			unsigned long msec = 0;
			msec += time[0];
			msec += time[1] << 8;
			msec += time[2] << 16;
			msec += time[3] << 24;

			std::cout << length - ipv4_hdr.header_length()
				<< " bytes from " << ipv4_hdr.source_address()
				<< ": icmp_seq=" << icmp_hdr.sequence_number()
				<< ", ttl=" << ipv4_hdr.time_to_live()
				<< " "
				<< boost::posix_time::millisec(msec)
				<< std::endl;
		}
	}

	void server_recv() {
		reply_buffer_.consume(reply_buffer_.size());

		socket_.async_receive(reply_buffer_.prepare(65536),
			boost::bind(&IcmpTimestamp::server_recv_handler, this, _2));
	}

	void server_recv_handler(std::size_t length)
	{
		reply_buffer_.commit(length);

		// Decode request
		std::istream is(&reply_buffer_);
		ipv4_header ipv4_hdr;
		icmp_header icmp_hdr;
		is >> ipv4_hdr >> icmp_hdr;

		if (icmp_hdr.type() == icmp_header::timestamp_request)
		{
			boost::asio::ip::icmp::endpoint destination(ipv4_hdr.source_address(), 0);

			std::vector<char> body(12);
			is >> body[0] >> body[1] >> body[2] >> body[3];

			std::vector<char> time(encodeCurrentTime());

			std::copy(time.begin(), time.end(), body.begin() + 4);
			std::copy(time.begin(), time.end(), body.begin() + 8);

			send(destination, 14, 0, body);
		}

		server_recv();
	}

	std::vector<char> encodeCurrentTime() {

		std::vector<char> time(4);

		boost::posix_time::ptime now  = boost::posix_time::second_clock::universal_time();
		boost::posix_time::time_duration diff = now.time_of_day();
		unsigned long long timeInMilliseconds = diff.total_milliseconds() + ULLONG_MAX + 1;

		time[3] = timeInMilliseconds & 0xFF;
		time[2] = (timeInMilliseconds >> 8) & 0xFF;	
		time[1] = (timeInMilliseconds >> 16) & 0xFF;	
		time[0] = (timeInMilliseconds >> 24) & 0xFF;	

		return time;
	}

	boost::asio::io_service io_service_;
	icmp::resolver resolver_;
	icmp::socket socket_;
	unsigned short sequence_number_;
	unsigned short identifier_;
	boost::asio::streambuf reply_buffer_;
};

int main(int argc, char* argv[]) {

	try
	{
		std::string usage("Usage: \nicmp_timestamp client <host>\nicmp_timestamp server");

		if (argc < 2)
		{
			std::cerr << usage << std::endl;
			return 1;
		}

		IcmpTimestamp icmpTimestamp;

		if (strcmp(argv[1], "server") == 0) {
			icmpTimestamp.server();
		}

		else if ((strcmp(argv[1], "client") == 0) && (argc >= 3)) {
			icmpTimestamp.client(argv[2]);
		}

		else {
			std::cerr << usage << std::endl;
			return 1;
		}

	}
	catch (std::exception& e)
	{
		std::cerr << "Exception: " << e.what() << std::endl;
		return 1;
	}

	return 0;
}
